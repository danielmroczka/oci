package com.dm.labs.oci.oci.storage;

import com.oracle.bmc.auth.AuthenticationDetailsProvider;
import com.oracle.bmc.objectstorage.ObjectStorage;
import com.oracle.bmc.objectstorage.ObjectStorageClient;
import com.oracle.bmc.objectstorage.requests.GetNamespaceRequest;
import com.oracle.bmc.objectstorage.requests.GetObjectRequest;
import com.oracle.bmc.objectstorage.requests.PutObjectRequest;
import com.oracle.bmc.objectstorage.responses.GetNamespaceResponse;
import com.oracle.bmc.objectstorage.responses.GetObjectResponse;
import com.oracle.bmc.objectstorage.transfer.UploadConfiguration;
import com.oracle.bmc.objectstorage.transfer.UploadManager;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@RestController
@RequestMapping("/image")
public class ObjectStorageController {

    final ObjectStorage client;

    final String namespaceName;

    final String bucketName = "bucket-20190927-2214";

    final AuthenticationDetailsProvider provider;

    public ObjectStorageController(AuthenticationDetailsProvider provider) throws IOException {
        this.provider = provider;
        client = new ObjectStorageClient(provider);
        GetNamespaceResponse namespaceResponse = client.getNamespace(GetNamespaceRequest.builder().build());
        namespaceName = namespaceResponse.getValue();
    }

//    @GetMapping
//    public List list() {
//        GetObjectResponse getResponse =
//                client.getObject(
//                        GetObjectRequest.builder()
//                                .namespaceName(namespaceName)
//                                .bucketName(bucketName)
//
//                                .build());
//    }

    @GetMapping(value = "/{name}")
    public @ResponseBody
    byte[] getImage(@PathVariable String name) throws Exception {
        GetObjectResponse getResponse =
                client.getObject(
                        GetObjectRequest.builder()
                                .namespaceName(namespaceName)
                                .bucketName(bucketName)
                                .objectName(name)
                                .build());
        byte[] bytes;

        try (final InputStream fileStream = getResponse.getInputStream()) {
            bytes = IOUtils.toByteArray(fileStream);
        }

        getResponse.getInputStream().close();

        return bytes;
    }

    @PostMapping
    String upload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
        UUID filename = UUID.randomUUID();

        UploadConfiguration uploadConfiguration =
                UploadConfiguration.builder()
                        .allowMultipartUploads(true)
                        .allowParallelUploads(true)
                        .build();
        UploadManager uploadManager = new UploadManager(client, uploadConfiguration);

        PutObjectRequest request =
                PutObjectRequest.builder()
                        .bucketName(bucketName)
                        .namespaceName(namespaceName)
                        .objectName(filename.toString())
                        .contentType("image/jpeg")
                        .build();


        UploadManager.UploadRequest uploadDetails =
                UploadManager.UploadRequest.builder(file.getInputStream(), file.getSize()).allowOverwrite(true).build(request);

        // upload request and print result
        // if multi-part is used, and any part fails, the entire upload fails and will throw BmcException
        UploadManager.UploadResponse response = uploadManager.upload(uploadDetails);
        System.out.println(response);

        // fetch the object just uploaded
        GetObjectResponse getResponse =
                client.getObject(
                        GetObjectRequest.builder()
                                .namespaceName(namespaceName)
                                .bucketName(bucketName)
                                .objectName(filename.toString())
                                .build());

        // use the response's function to print the fetched object's metadata
        System.out.println(getResponse.getOpcMeta());

        // stream contents should match the file uploaded
        try (final InputStream fileStream = getResponse.getInputStream()) {
            // use fileStream
        }

        return filename.toString();
    }

}
