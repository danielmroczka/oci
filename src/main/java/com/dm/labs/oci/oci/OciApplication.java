package com.dm.labs.oci.oci;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@RestController
public class OciApplication {

    private final Counter visitCounter;

    public OciApplication(MeterRegistry registry) {
        visitCounter = Counter.builder("visit_counter")
                .description("Number of visits to the site")
                .register(registry);
    }

    @GetMapping
    public String index(HttpServletRequest request) {
        visitCounter.increment();
        return "Hello World to " + request.getRemoteAddr();
    }

    public static void main(String[] args) {
        SpringApplication.run(OciApplication.class, args);
    }

}
