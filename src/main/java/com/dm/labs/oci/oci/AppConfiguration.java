package com.dm.labs.oci.oci;

import com.oracle.bmc.ConfigFileReader;
import com.oracle.bmc.auth.AuthenticationDetailsProvider;
import com.oracle.bmc.auth.ConfigFileAuthenticationDetailsProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class AppConfiguration {

    @Bean
    ConfigFileReader.ConfigFile configFile() throws IOException {
        return ConfigFileReader.parse("~/.oci/config");
    }

    @Bean
    AuthenticationDetailsProvider authenticationDetailsProvider() throws IOException {
        return new ConfigFileAuthenticationDetailsProvider(configFile());
    }
}
