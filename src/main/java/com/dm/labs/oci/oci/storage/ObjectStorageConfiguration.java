package com.dm.labs.oci.oci.storage;

import com.oracle.bmc.auth.AuthenticationDetailsProvider;
import com.oracle.bmc.objectstorage.ObjectStorage;
import com.oracle.bmc.objectstorage.ObjectStorageClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectStorageConfiguration {

    private final AuthenticationDetailsProvider provider;

    public ObjectStorageConfiguration(AuthenticationDetailsProvider provider) {
        this.provider = provider;
    }

    @Bean
    ObjectStorage client() {
        return new ObjectStorageClient(provider);
    }

}
